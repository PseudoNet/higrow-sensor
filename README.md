# HiGrow Plant Monitoring Sensor to MQTT Firmware

## Overview

- Use this code with HiGrow rev 1 board available [here](http://s.click.aliexpress.com/e/briQspLE)
![HiGrow](/images/HiGrow.png)

## How to install this app

- Install [mos tool](https://mongoose-os.com/software.html)

- mos build
- mos flash
- mos config-set wifi.sta.enable=true wifi.ap.enable=false wifi.sta.ssid=**SSID** wifi.sta.pass=**PASSWORD**
- mos config-set mqtt.enable=true mqtt.server=**SERVER_ADDRESS**:1883 mqtt.user=**USERNAME** mqtt.pass=**PASSWORD**
- mos config-set higrow.interval_min=60 (Sensor publish internval in minutes - Default 30)


## Additional
- More details on Generic MQTT setup https://mongoose-os.com/docs/cloud/mqtt.md
- mos config-set debug.level=3  //for extra logging

