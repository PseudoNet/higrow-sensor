load('api_config.js');
load('api_events.js');
load('api_gpio.js');
load('api_net.js');
load('api_sys.js');
load('api_timer.js');
load('api_esp32.js');
load('api_dht.js');
load('api_adc.js');
load('api_rpc.js');
load('api_mqtt.js');
load('api_log.js');

//Pins
let ResetPin = 0;
let LedPin = 16;
let DHTpin = 22;
let SOILpin = 32;
ADC.enable(SOILpin);
let dht = DHT.create(DHTpin, DHT.DHT11);
let isConnected = false;
let hasPublished = false;
let qos = 1;//Must be set to 1

Log.print(Log.INFO, 'Init');

function publishData() {

  let higrowdata = { deviceId: Cfg.get('higrow.id'), temperature: dht.getTemp(), humidity: dht.getHumidity(), water: ADC.read(SOILpin) };
  Log.print(Log.INFO, JSON.stringify(higrowdata));

  if (MQTT.pub('higrow/' + higrowdata.deviceId + '/temperature', JSON.stringify(higrowdata.temperature), qos)
    && MQTT.pub('higrow/' + higrowdata.deviceId + '/humidity', JSON.stringify(higrowdata.humidity), qos)
    && MQTT.pub('higrow/' + higrowdata.deviceId + '/water', JSON.stringify(higrowdata.water), qos)) {
    Log.print(Log.INFO, 'Payload sent');
    hasPublished = true;
  }
}


//Wait for mqtt server connection
Timer.set(10000, Timer.REPEAT, function () {
  if (isConnected) {
    Log.print(Log.DEBUG, 'Poll Connected');
    publishData();
  } else {
    Log.print(Log.DEBUG, 'No connecion...waiting');
  }
}, null
);

MQTT.setEventHandler(function (conn, ev) {
  if (ev !== 0) {
    let evs = '?';
    if (ev === MQTT.EV_CONNACK) {
      evs = 'CONNACK';   // Connection to broker has been established
    } else if (ev === MQTT.EV_PUBLISH) {
      evs = 'PUBLISH';   // msg published to topics we are subscribed to
    } else if (ev === MQTT.EV_PUBACK) {
      evs = 'PUBACK';    // ACK for publishing of a message with QoS&amp;gt;0
    } else if (ev === MQTT.EV_SUBACK) {
      evs = 'SUBACK';    // ACK for a subscribe request
    } else if (ev === MQTT.EV_UNSUBACK) {
      evs = 'UNSUBACK';  // ACK for an unsubscribe request
    } else if (ev === MQTT.EV_CLOSE) {
      evs = 'CLOSE';     // connection to broker was closed
    }
    Log.print(Log.DEBUG, 'MQTT event:' + evs);
    if (ev === MQTT.EV_CONNACK) {
      Log.print(Log.DEBUG, 'MQTT Connected');
      isConnected = true;
      publishData();
    } else if (ev === MQTT.EV_PUBACK) {
      if (hasPublished) {
        Log.print(Log.INFO, 'MQTT publish confirmed... init deep sleep');
        ESP32.deepSleep(Cfg.get('higrow.interval_min')*60000000);
      }
    }
  }
}, null);